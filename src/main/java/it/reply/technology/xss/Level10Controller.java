package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Level10Controller {

    public Level10Controller() { }

    @GetMapping("/level10") public String level10Form(Model model) {
        model.addAttribute("level10", new Level10());
        return "level10";
    }

    @PostMapping("/level10") public String level10Submit(@ModelAttribute Level10 level10, Model model) {
        model.addAttribute("level10", level10);
        return "level10";
    }

}
