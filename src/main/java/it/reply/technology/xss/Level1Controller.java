package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Level1Controller {

    public Level1Controller() { }

    @GetMapping("/level1") public String level1Form(Model model) {
        model.addAttribute("level1", new Level1());
        return "level1";
    }

    @PostMapping("/level1") public String level1Submit(@ModelAttribute Level1 level1, Model model) {
        model.addAttribute("level1", level1);
        return "level1";
    }

}
