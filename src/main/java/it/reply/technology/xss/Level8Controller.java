package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller public class Level8Controller {

    public Level8Controller() {
    }

    @GetMapping("/level8") public String level8Form(Model model) {
        model.addAttribute("level8", new Level8());
        return "level8";
    }

    @PostMapping("/level8") public String level8Submit(@ModelAttribute Level8 level8, Model model) {

        String content;
        while (level8.getContent().toLowerCase().contains("alert") || level8.getContent().toLowerCase().contains("eval")) {
            content = level8.getContent();
            content = content.replaceAll("(?i)alert", "");
            content = content.replaceAll("(?i)eval", "");
            level8.setContent(content);
        }

        model.addAttribute("level8", level8);
        return "level8";
    }

}
