package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.util.ArrayList;

@Controller
public class Level2Controller {

    Connection connection = DriverManager.getConnection("jdbc:h2:mem:testdb;USER=admin;PASSWORD=admin");
    Statement stmt = connection.createStatement();

    private ArrayList<Article> getArticles() throws SQLException {
        String query = "SELECT id, name, description FROM article";
        ResultSet rs = stmt.executeQuery(query);
        ArrayList<Article> articles = new ArrayList<>();
        while (rs.next()) {
            articles.add(new Article(rs.getString(2), rs.getString(3)));
        }
        return articles;
    }

    public Level2Controller() throws SQLException { }

    @GetMapping("/level2") public String level2Form(Model model) throws SQLException {
        Level2 level2 = new Level2();
        level2.setArticles(getArticles());
        model.addAttribute("level2", level2);
        return "level2";
    }

    @PostMapping("/level2") public String level2Submit(@ModelAttribute Level2 level2, Model model) throws SQLException {
        String query = "INSERT INTO article(name, description) VALUES ('" + level2.getItem() + "','" + level2.getDescription() + "')";

        stmt.execute(query);
        level2.setArticles(getArticles());
        model.addAttribute("level2", level2);
        return "level2";
    }

}
