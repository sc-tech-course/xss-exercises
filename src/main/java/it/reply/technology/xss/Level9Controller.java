package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;

@Controller public class Level9Controller {

    public Level9Controller() {
    }

    @GetMapping("/level9") public String level9Form(Model model) {
        model.addAttribute("level9", new Level9());
        return "level9";
    }

    @PostMapping("/level9") public String level9Submit(@ModelAttribute Level9 level9, Model model) {

        String[] blacklist = {"alert", "script", "eval", "function", "fun"};

        while (Arrays.stream(blacklist).anyMatch(level9.getContent().toLowerCase()::contains)) {
            String content = level9.getContent();
            String match = Arrays.stream(blacklist).filter(level9.getContent().toLowerCase()::contains).findAny().get();
            content = content.replaceAll("(?i)" + match, "");
            level9.setContent(content);
        }

        model.addAttribute("level9", level9);
        return "level9";
    }

}
