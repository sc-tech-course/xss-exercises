package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller public class Level4Controller {

    public Level4Controller() {
    }

    @GetMapping("/level4") public String level4Form(Model model) {
        model.addAttribute("level4", new Level4());
        return "level4";
    }

    @PostMapping("/level4") public String level41Submit(@ModelAttribute Level4 level4, Model model) {

        String content = level4.getContent();
        content = content.replaceAll("<script>", "");
        content = content.replaceAll("</script>", "");
        level4.setContent(content);

        model.addAttribute("level4", level4);
        return "level4";
    }

}
