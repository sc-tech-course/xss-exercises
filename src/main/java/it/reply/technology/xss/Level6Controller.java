package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller public class Level6Controller {

    public Level6Controller() {
    }

    @GetMapping("/level6") public String level6Form(Model model) {
        model.addAttribute("level6", new Level6());
        return "level6";
    }

    @PostMapping("/level6") public String level6Submit(@ModelAttribute Level6 level6, Model model) {

        String content = level6.getContent();
        while (level6.getContent().toLowerCase().contains("<script>") || level6.getContent().toLowerCase().contains("</script>")) {
            content = level6.getContent();
            content = content.replaceAll("(?i)<script>", "");
            content = content.replaceAll("(?i)</script>", "");
            level6.setContent(content);
        }

        model.addAttribute("level6", level6);
        return "level6";
    }

}
