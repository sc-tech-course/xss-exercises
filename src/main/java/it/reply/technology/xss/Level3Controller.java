package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class Level3Controller {

    public Level3Controller() { }

    @GetMapping("/level3") public String level3Form(Model model) {
        return "level3";
    }

}
