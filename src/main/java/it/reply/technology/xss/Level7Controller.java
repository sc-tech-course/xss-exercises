package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller public class Level7Controller {

    public Level7Controller() {
    }

    @GetMapping("/level7") public String level7Form(Model model) {
        model.addAttribute("level7", new Level7());
        return "level7";
    }

    @PostMapping("/level7") public String level7Submit(@ModelAttribute Level7 level7, Model model) {

        String content;
        while (level7.getContent().toLowerCase().contains("alert")) {
            content = level7.getContent();
            content = content.replaceAll("(?i)alert", "");
            level7.setContent(content);
        }

        model.addAttribute("level7", level7);
        return "level7";
    }

}
