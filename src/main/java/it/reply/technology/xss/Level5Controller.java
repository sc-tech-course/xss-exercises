package it.reply.technology.xss;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller public class Level5Controller {

    public Level5Controller() {
    }

    @GetMapping("/level5") public String level5Form(Model model) {
        model.addAttribute("level5", new Level5());
        return "level5";
    }

    @PostMapping("/level5") public String level51Submit(@ModelAttribute Level5 level5, Model model) {

        String content = level5.getContent();
        while (level5.getContent().contains("<script>") || level5.getContent().contains("</script>")) {
            content = level5.getContent();
            content = content.replaceAll("<script>", "");
            content = content.replaceAll("</script>", "");
            level5.setContent(content);
        }

        model.addAttribute("level5", level5);
        return "level5";
    }

}
