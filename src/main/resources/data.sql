DROP TABLE IF EXISTS login;

CREATE TABLE login (
  id INT AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(250) NOT NULL,
  password VARCHAR(250) NOT NULL,
  admin BOOLEAN NOT NULL
);

INSERT INTO login(username, password, admin) VALUES
('admin', 'asdgvf734i', true),
('user1', 'ucrhadFFFEAfGA', false);

DROP TABLE IF EXISTS article;

CREATE TABLE article (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  description VARCHAR(250) NOT NULL,
  category INT
);

INSERT INTO article(name, description, category) VALUES
('AMD Ryzen 3600X', 'CPU 6C/12T 3.8 GHz', 1),
('AMD Ryzen 3700X', 'CPU 8C/16T 3.6 GHz', 1),
('NVIDIA RTX 3070', 'GPU 11 GB DDR6', 2);

DROP TABLE IF EXISTS message;

CREATE TABLE message (
    id INT AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(250) NOT NULL,
    content VARCHAR(4000) NOT NULL
)